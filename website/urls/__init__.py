# The website url settings all
# url definitions cam be included
# here.
from django.conf.urls import include, url


urlpatterns = [
    url(r'^', include('website.urls.plugins')),
    url(r'^', include('website.urls.webapps')),
]
