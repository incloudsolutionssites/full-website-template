from django.conf.urls import include, url


urlpatterns = [
    url(r'^agenda/',         include('sbcalendar.urls', namespace='calendar')),
    url(r'^contato/',        include('tinybox.core.contact.urls', namespace='contact')),
    url(r'^galeria/',        include('sbgallery.urls', namespace='gallery')),
    url(r'^noticias/',       include('sbblog.urls', namespace='blog')),

    url(r'^ckeditor/',       include('ckeditor_uploader.urls')),
]
