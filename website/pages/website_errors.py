from django.conf import settings as django_settings
from tinybox.core.pages import Library


library = Library()


if django_settings.DEBUG:
    # ------------- 404 ERROR ------------- #
    # http://localhost:8000/404/
    library.create_page(
        'page-not-found', 'Page Not Found',
        template_name='404.html', regex=r'^404/',
        show_in_menu=False)

    # ------------- 500 ERROR ------------- #
    # http://localhost:8000/500/
    library.create_page(
        'internal-error', 'Internal Server Error',
        template_name='500.html', regex=r'^500/',
        show_in_menu=False)

    # ------------- 403 ERROR ------------- #
    # http://localhost:8000/403/
    library.create_page(
        'forbidden', 'Forbidden',
        template_name='403.html', regex=r'^403/',
        show_in_menu=False)
