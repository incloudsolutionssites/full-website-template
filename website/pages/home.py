# -*- coding: utf-8 -*-
from tinybox.core import pages


library = pages.Library()


@library.register(name="home", url_regex="^$")
class HomePage(pages.Page):
    title = u"Início"
    position = 1
