# -*- coding: utf-8 -*-
from tinybox.core import pages


library = pages.Library()


@library.register(name="ensino")
class EnsinoPage(pages.Page):
    title = u"Ensino"
    allow_view = False
    position = 3


@library.register(name="educacao-infantil")
class EducacaoInfantilPage(pages.Page):
    title = u"Educação Infantil"
    child_of = "ensino"
