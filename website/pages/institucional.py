# -*- coding: utf-8 -*-
from tinybox.core import pages


library = pages.Library()


@library.register(name="institucional")
class InstitucionalPage(pages.Page):
    title = u"Escola"
    allow_view = False
    position = 2


@library.register(name="sobre")
class SobrePage(pages.Page):
    title = u"Sobre"
    child_of = "institucional"
