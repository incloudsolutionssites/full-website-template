from django.conf import settings as django_settings
from tinybox.core.config.settings import UNDER_CONSTRUCTION_TEMPLATE
from tinybox.core.config.settings import UNDER_MAINTENANCE_TEMPLATE
from tinybox.core.pages import Library


library = Library()


if django_settings.DEBUG:
    # ------------- CONSTRUCTION ------------- #
    # http://localhost:8000/under/construction/
    library.create_page(
        'under-construction', 'Under Construction',
        template_name=UNDER_CONSTRUCTION_TEMPLATE, regex=r'^under/construction/',
        show_in_menu=False)

    # -------------- MAINTENANCE ------------- #
    # http://localhost:8000/under/maintenance/
    library.create_page(
        'under-maintenance', 'Under Maintenance',
        template_name=UNDER_MAINTENANCE_TEMPLATE, regex=r'^under/maintenance/',
        show_in_menu=False)
