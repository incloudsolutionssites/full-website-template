from django.test import TestCase
from django.test.client import RequestFactory
from tinybox.core.pages.base import _library


class SimplePageTest(TestCase):
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
        self.pages = _library.get_pages()

    def _response(self, page):
        # create a request to view test
        request = self.factory.get(page.get_absolute_url())

        # get page view
        page_view = page.view

        # make a response to view
        response = page_view(request, **{"extra_context": page.get_context_data()})

        return response

    def _test_response(self, page, _lvl):
        nested = '    ' * _lvl
        msg = "%(nested)sTesting page ``%(page)s`` %(dot)s %(status)s "

        response = self._response(page)

        # dump output
        print msg % {
            'nested': nested,
            'page': page.name,
            'dot': "." * (60 - len(page.name) - (_lvl * 4)),
            'status': response.status_code
        }

        self.assertEqual(response.status_code, 200)

    def _http_response(self, pages, _lvl=0):
        for page in pages:
            children = page.get_children()

            if not children:
                self._test_response(page, _lvl)
                continue

            if page.allow_view:
                self._test_response(page, _lvl)
            else:
                print 'Testing nested pages of ``%(page)s``' % {'page': page.name}

            self._http_response(children, _lvl=_lvl + 1)

    def test_response(self):
        self._http_response(self.pages)

