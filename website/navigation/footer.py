# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils.functional import curry
from tinybox.core.navigation import Navigation
from tinybox.core.navigation import NavigationNode
from tinybox.core.navigation import library as navigation_library


@navigation_library.register(namespace='footer')
class FooterSitemapNavigation(Navigation):
    def get_nodes(self, request):
        nodes = super(FooterSitemapNavigation, self).get_nodes(request)
        node = curry(NavigationNode, request=request)
        return nodes + [
            node(u'Início',             reverse('pages:home')),
            node(u"Notícias",           reverse('blog:index')),
            node(u"Agenda",             reverse('calendar:index')),
            node(u"Galeria",            reverse('gallery:index')),
            node(u'Contato',            reverse('contact:index')),
        ]
