# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils.functional import curry
from tinybox.core.navigation import NavigationNode
from tinybox.core.navigation import library as navigation_library
from tinybox.core.pages import PageNavigation


@navigation_library.register(namespace='main')
class MainNavigation(PageNavigation):
    def get_nodes(self, request):
        nodes = super(MainNavigation, self).get_nodes(request)
        node = curry(NavigationNode, request=request)
        return nodes + [
            node(u"Notícias",   reverse('blog:index'), regex='^/noticias/'),
            node(u"Agenda",     reverse('calendar:index'), regex='^/agenda/'),
            node(u"Galeria",    reverse('gallery:index'), regex='^/galeria/'),
            node(u"Contato",    reverse('contact:index'), regex='^/contato/'),
        ]
