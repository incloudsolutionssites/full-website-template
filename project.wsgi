import os
import sys

# Calculates the path based on location of WSGI script.
apache_configuration = os.path.realpath(os.path.dirname(__file__))
project = os.path.join(apache_configuration, '%(app)s')

sys.path.append(apache_configuration)
sys.path.append(project)
sys.path.insert(0, '%(virtual_path)s/lib/python2.7/site-packages')

os.environ['PYTHON_EGG_CACHE'] = '%(app_directory)s/.python-eggs'
os.environ['DJANGO_SETTINGS_MODULE'] = 'src.settings.production'

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()