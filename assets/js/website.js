// position when the affix element stop on bottom
var AFFIX_BOTTOM = $('footer').outerHeight(true);

// fix position of padding or margin off inside elements
var AFFIX_FIX_TOP = 15;
var AFFIX_FIX_BOTTOM = 15;


/* ========================================================================
 * Bootstrap: scroll.js v1.0.0
 * ========================================================================
 * Copyright 2011-2017 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
    'use strict';

    // SCROLL CLASS DEFINITION
    // ======================

    var Scroll = function (element, options) {
        this.options = $.extend({}, Scroll.DEFAULTS, options)

        this.$element = $(element)
        var self = this

        this.$element.on('click', function (e) {
            e.stopPropagation();
            self.scroll();
            return false;
        })
    }

    Scroll.VERSION  = '1.0.0'

    Scroll.DEFAULTS = {
        to: null,
        speed: 500,
        container: 'html, body'
    }

    Scroll.prototype.getTo = function () {
        var to = this.options.to
        if (/^\d+$/.exec(to)) return to

        var target = (to === null ? this.$element.attr('href') : to)
        if (!target) return null
        else if (!$(target).length) return null

        // Update the address bar
        window.history.pushState({}, '', target);

        return $(target).offset().top
    }

    Scroll.prototype.scroll = function () {
        var to = this.getTo()
        if (to === null) return
        $(this.options.container).animate({scrollTop: to}, this.options.speed)
    }

    // SCROLL PLUGIN DEFINITION
    // =======================

    function Plugin(option) {
        return this.each(function () {
            var $this   = $(this)
            var data    = $this.data('sb.scroll')
            var options = typeof option == 'object' && option

            if (!data) $this.data('sb.scroll', (data = new Scroll(this, options)))
            if (typeof option == 'string') data[option]()
        })
    }

    $.fn.scroll             = Plugin
    $.fn.scroll.Constructor = Scroll


    // SCROLL DATA-API
    // ==============

    $(window).on('load', function () {
        $('[data-scroll="slide"]').each(function () {
            var $scroll = $(this)
            var data = $scroll.data()
            Plugin.call($scroll, data)
        })
    })

}(jQuery);


/* ========================================================================
 * Bootstrap: auto-width.js v1.0.0
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {

    // AUTO-WIDTH DATA-API
    // ==============

    $(window).on('load', function () {
        $('[data-rel="auto-width"]').each(function () {
            var $element = $(this)
            var data = $element.data()
            var container = $(data.container)
            var $container = (container.length ? container : $element.parent())

            var resizeElement = function () {
                $element.css({"width": $container.width()})
            }

            $(window).resize(function() { resizeElement() })

            resizeElement()
        })
    })

}(jQuery);


/* ========================================================================
 * Bootstrap: affix.js v1.0.0
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {

    // AFFIX DATA-API
    // ==============

    $(window).on('load', function () {
        $('[data-container="affix"]').each(function () {
            // affix based in relative container
            var $element = $(this)
            var data = $element.data()
            var offset = data.offset || {}

            offset.top       = data.offsetTop || $element.offset().top + AFFIX_FIX_TOP
            offset.bottom    = data.offsetBottom || AFFIX_BOTTOM + AFFIX_FIX_BOTTOM

            $element.affix({offset: offset})
        })
    })

}(jQuery);


/* ========================================================================
 * Bootstrap: tooltips.js v1.0.0
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {

    // TOOLTIP CLASS BASED
    // ===================

    $(window).on('load', function () {
        $('.tooltip-left').tooltip({ placement: 'left'})
        $('.tooltip-top').tooltip({ placement: 'top'})
        $('.tooltip-right').tooltip({ placement: 'right'})
        $('.tooltip-bottom').tooltip({ placement: 'right'})
    })

}(jQuery);


/* ========================================================================
 * Bootstrap: tooltips.js v1.0.0
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {

    // TOOLTIP CLASS BASED
    // ===================

    $(window).on('load', function () {
        $('.tip').tooltip({ placement: 'auto'})
        $('.tip-left').tooltip({ placement: 'left'})
        $('.tip-top').tooltip({ placement: 'top'})
        $('.tip-right').tooltip({ placement: 'right'})
        $('.tip-bottom').tooltip({ placement: 'bottom'})
    })

}(jQuery);