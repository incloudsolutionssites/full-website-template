import ConfigParser
import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

SETTINGS_FILE = os.path.join(BASE_DIR, 'project.ini')


class Section(object):
    def __init__(self, name, data):
        self._name = name
        self.data = data

    def __repr__(self):
        return '<Settings:Section %s>' % self._name

    def __getattr__(self, item):
        try:
            return self.data[item]
        except KeyError:
            return None


class Settings(object):
    def __init__(self, settings_file):
        self._settings_file = settings_file

        # Init config parser object with file path
        self._settings = ConfigParser.ConfigParser()
        self._settings.read(settings_file)

        self._sections = {}

    def __getattr__(self, item):
        try:
            super(Settings, self).__getattribute__(item)
        except AttributeError, e:
            if item in self._settings.sections():
                return self.section(item)
            else:
                raise AttributeError(e.message)

    def __repr__(self):
        return '<Settings from %s>' % self._settings_file

    def section(self, section):
        if section not in self._sections:
            self._sections[section] = Section(section, dict(self._settings.items(section)))
        return self._sections[section]


class DjangoSettings(Settings):
    def _get_allowed_hosts(self):
        project = self.section('project')

        def www(domain):
            return 'www.%s' % domain

        hosts = [
            project.domain,
            project.domain_alternative,

            # add www prefix
            www(project.domain),
            www(project.domain_alternative)
        ]
        return hosts

    allowed_hosts = property(_get_allowed_hosts)

    def _get_databases(self):
        # parse django database settings
        # dict([(key.upper(), value) for key, value in settings.database.items()])
        sections = [section for section in self._settings.sections() if section.startswith('database')]
        databases = {}

        def database(_db):
            return {
                'ENGINE': _db.engine,
                'NAME': _db.name,
                'USER': _db.user,
                'PASSWORD': _db.password,
                'HOST': _db.host,
                'PORT': _db.port,
            }

        for db in sections:
            if ':' not in db:
                databases['default'] = database(self.section(db))
                continue

            # get another database settings
            databases[db.split(':')[1]] = database(self.section(db))

        return databases

    databases = property(_get_databases)


settings = DjangoSettings(SETTINGS_FILE)
