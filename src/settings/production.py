"""
Django settings for {{ project_name }} project.

Generated by 'django-admin startproject' using Django 1.9.7.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""
import os
from src.config import settings as project_settings
from split_settings.tools import include


if os.environ['DJANGO_SETTINGS_MODULE'] == 'src.settings.production':
    # must bypass this block if another settings module was specified
    include("base.py", scope=locals())

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = project_settings.allowed_hosts

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

USE_TZ = True

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = project_settings.databases

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

MEDIA_URL = '/static/media/'

MEDIA_ROOT = project_settings.server.media_root

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = project_settings.server.static_root
