from django.contrib.auth import get_user_model
from django.db.models.signals import post_migrate
from django.dispatch.dispatcher import receiver


@receiver(post_migrate)
def add_super_admin(*args, **kwargs):
    """ Add super user on first syncdb """
    User = get_user_model()

    if not User.objects.filter(is_staff=True).exists():
        User.objects.create_superuser(
            username="{{ project_name }}",
            password="{{ project_name }}",
            email="webmaster@systembrasil.com.br"
        )
