#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# On Debian systems, you can find the full text of the license in
# /usr/share/common-licenses/GPL-2

from fabric.api import *
from src.config import settings as project_settings

import sys
import os
import datetime
import time


BASE_HEADER = """%(now)s
using project '%(app)s'
%(desc)s
"""


# -------------------------------------- #
# ------------ GIT BRANCHES ------------ #
# -------------------------------------- #

def pull(git_branch='master'):
    """
    Only on local, to change branches and pull
    :param git_branch:
        what branch to pull.
    """
    env.git_origin = project_settings.project.git_origin
    env.git_branch = git_branch


# -------------------------------------- #
# ------- AVAILABLE ENVIRONMENTS ------- #
# -------------------------------------- #

def localhost():
    env.app = project_settings.project.name
    env.app_root = os.path.dirname(__file__)
    env.virtual = os.path.join(env.app_root, '.virtualenv')
    env.pip = os.path.join(env.app_root, '.virtualenv', "bin", "pip")
    env.python = os.path.join(env.app_root, '.virtualenv', "bin", "python")


def production():
    env.app = project_settings.project.name

    # server settings
    env.host = project_settings.server.host
    env.user = project_settings.server.user
    env.hosts = ['%s@%s' % (env.user, env.host)]
    env.server_alias = project_settings.server.alias
    env.app_directory = '/home/%s/apps_wsgi' % env.user
    env.app_root = '%s/%s' % (env.app_directory, env.app)

    # project settings
    env.django_settings = 'src.settings.production'
    env.domain = project_settings.project.domain
    env.manage = env.app_root + '/manage.py'

    # git settings
    env.git_origin = project_settings.project.git_origin
    env.git_branch = project_settings.project.git_branch

    # virtualenv settings
    env.virtual_path = '/home/%s/.virtualenvs/%s' % (env.user, env.domain)
    env.virtual = '%s/bin/activate' % env.virtual_path


# -------------------------------------- #
# --------- AVAILABLE COMMANDS --------- #
# -------------------------------------- #

def provision():
    # starts provision
    start = time.time()

    # validate environment
    if not hasattr(env, 'app_root'):
        print 'ERROR: unknown environment.'
        os.sys.exit(1)

    sys.stdout.write(BASE_HEADER % {
        "now": datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p"),
        "app": env.app, "desc": "Provision local developer enviroment"
    })

    with lcd(env.path):
        # Create virtualenv if not exists
        if not os.path.exists(env.virtual):
            command = "virtualenv %s" % env.virtual
            local(command)

        # allow to update repository
        if getattr(env, 'git_branch', None):
            # clone repository
            command = 'test -d %s/.git || git clone %s %s -b %s'
            local(command % (env.app_root, env.git_origin, env.app_root, env.git_branch))

            # update repository
            command = 'git checkout %s && git pull origin %s'
            local(command % (env.git_branch, env.git_branch))

        # Run install in requirements
        command = "%(pip)s install -r requirements.txt"
        local(command % env)

        # Run database migrations
        command = "%(python)s manage.py migrate"
        local(command % env)

        # clear directory
        command = 'find %s -name "*.pyc" -delete'
        local(command % env.app_root)

    final = time.time()
    puts('\nexecution finished in %.2fs' % (final - start))


def deploy():
    # starts deploy
    start = time.time()

    # validate environment
    if not hasattr(env, 'app_root'):
        print 'ERROR: unknown environment.'
        os.sys.exit(1)

    sys.stdout.write(BASE_HEADER % {
        "now": datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p"),
        "app": env.app, "desc": "App deploy"
    })

    # clone repository
    command = 'test -d %s/.git || (rm -Rf %s; git clone %s %s -b %s)'
    run(command % (env.app_root, env.app_root, env.git_origin, env.app_root, env.git_branch))

    # update repository
    command = 'cd "%s" && git reset --hard && git pull && git checkout -B %s origin/%s && git pull'
    run(command % (env.app_root, env.git_branch, env.git_branch))

    # update python package
    command = 'source %s; cd %s; pip install -r requirements.txt'
    run(command % (env.virtual, env.app_root))

    # update static media
    command = 'source %s; python %s collectstatic --noinput --settings=%s'
    run(command % (env.virtual, env.manage, env.django_settings))

    # run migrate
    command = 'source %s; python %s migrate --noinput --settings=%s'
    run(command % (env.virtual, env.manage, env.django_settings))

    # restart uwsgi service
    # run('find %s -name "*.pyc" -delete' % env.app_root)
    # run('/etc/init.d/uwsgi reload %s' % env.domain)
    run('touch %s.wsgi' % env.app_root)

    final = time.time()
    puts('execution finished in %.2fs' % (final - start))


# -------------------------------------- #
# ----------- SETUP COMMANDS ----------- #
# -------------------------------------- #

def kinghost():

    # validate environment
    if not hasattr(env, 'app_root'):
        print 'ERROR: unknown environment.'
        os.sys.exit(1)

    # create ssh key
    command = (
        'test -e /home/%s/.ssh/id_rsa.pub || ('
        'ssh-keygen -t rsa;'
        'cat /home/%s/.ssh/id_rsa.pub)')
    run(command % (env.user, env.user))

    # add current machine into .ssh/authorized_keys
    command = (
        'cat ~/.ssh/id_rsa.pub | '
        'ssh %s@%s \'test -e ~/.ssh/authorized_keys || cat >> ~/.ssh/authorized_keys\'; '
        'ssh %s@%s \'chmod 600 ~/.ssh/authorized_keys\'')
    local(command % (env.user, env.host, env.user, env.host))

    # add jenkins machine into .ssh/authorized_keys
    JENKINS_SSH = (
        'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9PK2UxCx8x5QqIX6bBDNI2b/gZ+nCQvGkRes'
        'arNa/TtuzLZBpcEDep2/nhdBKOlTHgD3jDh0XxjmEV0vkMhQ9ixU0ZfWbyDjfrfVMZG/hoMM2yf'
        '9cQb5ihE3tqukSHOcXdgue/mzzEQPeyDQaupipG102moc4rqf9G9lP6BSERsIBVB6N/eiT6KJBZ'
        'UfumAKsltOoYF8LQ0s6bCViqN9PG4czaK0CL0BfU9W/mzfqjYw7aefIhUCxBz54OOL/PttoC8Gc'
        'unIP79coH9mkldBpWvRr/FFgf6iEUQalXJ8qCZ+yad6E3vMbLrKFlvT19PWCz/TT52Jp35VGRLM'
        'ti8TrY+ONa6EPnAe3HUw83C3APXXSD0HU64q8G3PFComjJGFSVpf6YrziDACw4mwjx2cjNCMrYQ'
        'ol9Lmf6zbFkQmN4IiD2PUIiJP+Iq6h/y9kpspWgNFsF00M17lUiyUdBTfXCBWuSgkvklJPVQZu1'
        'peL1GIHlANlDBG6tz1uiuXkcZ/f/d+BLnfd0mfc+8zYLzIMndePhCc2ilQCsh7f1q0KN/nP5sb3'
        'RkEK3saVwTCgJxiDysnjsYgd/qsy2AWoCwNT0RiA8YYJ8K4DnZXqjhkWarP4O2Zz32KPFZilsrh'
        'G1uuFzkRLSwjwh7658GpAxB/20HwGY/E+zRIdBKdEOhRoRw== dev@systembrasil.com.br')

    command = (
        'ssh %s@%s \'echo \"%s\" >> ~/.ssh/authorized_keys\'; '
        'ssh %s@%s \'chmod 600 ~/.ssh/authorized_keys\'')
    local(command % (env.user, env.host, JENKINS_SSH, env.user, env.host))

    # update python path
    command = 'export PYTHONPATH="%s/.site-packages/"'
    run(command % env.app_directory)

    # install virtualenv
    command = (
        'test -d /home/%s/apps_wsgi/.site-packages || ('
        'mkdir /home/%s/apps_wsgi/.site-packages; '
        'easy_install -a --install-dir=$PYTHONPATH virtualenv)')
    run(command % (env.user, env.user))

    # create virtualenvs folder
    command = 'test -d /home/%s/.virtualenvs/ || mkdir /home/%s/.virtualenvs/'
    run(command % (env.user, env.user))

    # create app virtualenv
    command = 'test -d %s || python %s/.site-packages/virtualenv %s'
    run(command % (env.virtual_path, env.app_directory, env.virtual_path))

    # create static folder
    command = 'test -d %s || mkdir %s'
    run(command % (project_settings.server.static_root, project_settings.server.static_root))

    # update wsgi file
    wsgi_file = os.path.join(os.path.dirname(__file__), 'project.wsgi')
    wsgi_content = open(wsgi_file, 'r').read()
    wsgi_content = wsgi_content % env
    command = 'echo "%s" > %s/%s.wsgi'
    run(command % (wsgi_content, env.app_directory, env.app))


def setup():
    # starts prepare
    start = time.time()

    # validate environment
    if not hasattr(env, 'app_root'):
        print 'ERROR: unknown environment.'
        os.sys.exit(1)

    sys.stdout.write(BASE_HEADER % {
        "now": datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p"),
        "app": env.app, "desc": "App Setup"
    })

    if env.server_alias == 'kinghost':
        kinghost()

    final = time.time()
    puts('execution finished in %.2fs' % (final - start))
